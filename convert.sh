#!/bin/sh

# IMPORTANT! should give permissions to the file to be able to run it `chmod +x ./[convert].sh`
# run it passing name of the mp4 to convert, and name of the gif to be

# # crop (time)
# ffmpeg -i onvimeo.mp4 -to 00:00:13 -c copy output.cropped.mp4

# # scale (size of heigh)
# ffmpeg -i output.cropped.mp4 -filter:v "crop=815:415:0:0" -c:a copy output.sized.mp4

# # convert to gif
# ffmpeg -i output.sized.mp4 -vf "fps=7,scale=600:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif

# # remove intermediate files
# rm -r output.cropped.mp4
# rm -r output.sized.mp4

if [ -z "$1" ];
then
    FROM=input
    TO=output
    echo "\$1 or \$2 are empty"
else
    FROM=$1
    TO=$2
    echo "$1 and $2 are NOT empty"
fi

# crop (time)
# ffmpeg -i onvimeo.mp4 -to 00:00:13 -filter:v "crop=815:415:0:0" -c:a copy  output.cropped.sized.mp4
# ffmpeg -i "$FROM".mp4 -to 00:00:13 -filter:v "crop=815:415:0:0" -c:a copy  output.cropped.sized.mp4
ffmpeg -i "$FROM".mp4 -to 00:00:13 -filter:v "crop=910:465:0:0" -c:a copy  output.cropped.sized.mp4

# convert to gif
# ffmpeg -i output.cropped.sized.mp4 -vf "fps=7,scale=600:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif
ffmpeg -i output.cropped.sized.mp4 -vf "fps=7,scale=600:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 "$TO".gif

# remove intermediate files
rm -r output.cropped.sized.mp4

