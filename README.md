
<h1 align="center">Mp4ToGif</h1>

<!-- GETTING STARTED -->
## Getting Started

This is a repository to convert videos from .mp4 format to .gif

### Prerequisites

In order to make this repo work in your computer you must have installed `ffmpeg` first.
[https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/wiki/Installing-ffmpeg-on-Mac-OS-X]
* install ffmpeg in your mac using brew.
  ```sh
  $ brew install ffmpeg
  ```

### Install

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ca.reyes/mp4togif
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Install NPM packages
   ```sh
   cd mp4togif
   ```
4. Also you need to move your .mp4 inside this root repo folder

<!-- USAGE EXAMPLES -->
## Usage

For the use of this repo, there is one script you have to run passing two arguments, the name of the input and the output, both without the extensions
1. the name of the .mp4 you want convert from (input)
2. the name of the .gif resulting of the convert (output)

`./convert.sh [name of the mp4] [name of the gif]`

Sample for convert the .mp4 to .gif
```sh
   ./convert.sh carlos-mp4 carlos-gif 
   ```

